package com.trainingjourney.leo.asanarebelcodechallenge;

import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.controller.repoinfo.RepoInfoController;
import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.controller.repoinfo.RepoInfoControllerCallback;
import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.controller.subscriber.SubscriberController;
import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.controller.subscriber.SubscriberControllerCallback;
import com.trainingjourney.leo.asanarebelcodechallenge.ui.repodetails.RepoDetailsContract;
import com.trainingjourney.leo.asanarebelcodechallenge.ui.repodetails.RepoDetailsPresenterImpl;
import com.trainingjourney.leo.asanarebelcodechallenge.ui.search.adapter.Repo;
import com.trainingjourney.leo.asanarebelcodechallenge.util.connectionhelper.InternetConnectionHelper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static junit.framework.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class RepoDetailsPresenterUnitTest {

    @Mock
    RepoDetailsContract.View mockView;

    @Mock
    RepoInfoController mockRepoInfoController;

    @Mock
    SubscriberController mockSubscriberController;

    @Mock
    InternetConnectionHelper internetConnectionHelper;

    ArgumentCaptor<RepoInfoControllerCallback> repoInfoControllerCallbackArgumentCaptor;
    ArgumentCaptor<SubscriberControllerCallback> subscriberControllerCallbackArgumentCaptor;
    ArgumentCaptor<Repo> repoArgumentCaptor;

    Repo repo;
    RepoDetailsPresenterImpl presenter;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        presenter = new RepoDetailsPresenterImpl(mockView, mockRepoInfoController, mockSubscriberController, internetConnectionHelper);

        repoInfoControllerCallbackArgumentCaptor = ArgumentCaptor.forClass(RepoInfoControllerCallback.class);
        subscriberControllerCallbackArgumentCaptor = ArgumentCaptor.forClass(SubscriberControllerCallback.class);
        repoArgumentCaptor = ArgumentCaptor.forClass(Repo.class);

        repo = new Repo(1, "url", "name", "des", 1, "url", "url");
    }

    @Test
    public void testInternetConnectionChecking() {
        when(internetConnectionHelper.isInternetAvailable()).thenReturn(false);

        presenter.onInit(repo);

        verify(internetConnectionHelper).isInternetAvailable();
        verify(mockView,never()).showWaitingView();
        verify(mockView,never()).initViews(repo);
        verify(mockRepoInfoController,never()).getRepoInfo(anyString(), repoInfoControllerCallbackArgumentCaptor.capture());
        verify(mockSubscriberController,never()).getRepoSubscribers(anyString(), anyInt(), subscriberControllerCallbackArgumentCaptor.capture());

        assertEquals(0, presenter.getPageNumber());
    }


    @Test
    public void testOnInit() {
        when(internetConnectionHelper.isInternetAvailable()).thenReturn(true);

        presenter.onInit(repo);

        verify(internetConnectionHelper).isInternetAvailable();
        verify(mockView).showWaitingView();
        verify(mockView).initViews(repo);
        verify(mockRepoInfoController).getRepoInfo(anyString(), repoInfoControllerCallbackArgumentCaptor.capture());
        verify(mockSubscriberController).getRepoSubscribers(anyString(), anyInt(), subscriberControllerCallbackArgumentCaptor.capture());

        assertEquals(1, presenter.getPageNumber());
    }

    @Test
    public void testOnLoadData() {
        when(internetConnectionHelper.isInternetAvailable()).thenReturn(true);

        presenter.onInit(repo);
        presenter.onLoadData();

        verify(internetConnectionHelper,times(2)).isInternetAvailable();
        verify(mockSubscriberController, times(2)).getRepoSubscribers(anyString(), anyInt(), subscriberControllerCallbackArgumentCaptor.capture());
        assertEquals(2, presenter.getPageNumber());
    }


}