package com.trainingjourney.leo.asanarebelcodechallenge;

import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.controller.search.SearchController;
import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.controller.search.SearchControllerCallback;
import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.entity.Owner;
import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.entity.SearchResult;
import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.entity.SearchResultItem;
import com.trainingjourney.leo.asanarebelcodechallenge.ui.search.SearchContract;
import com.trainingjourney.leo.asanarebelcodechallenge.ui.search.SearchPresenterImpl;
import com.trainingjourney.leo.asanarebelcodechallenge.ui.search.adapter.Repo;
import com.trainingjourney.leo.asanarebelcodechallenge.util.connectionhelper.InternetConnectionHelper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;

import static junit.framework.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SearchPresenterUnitTest {
    @Mock
    SearchContract.View mockView;

    @Mock
    SearchController mockSearchController;

    @Mock
    InternetConnectionHelper internetConnectionHelper;

    ArgumentCaptor<SearchControllerCallback> searchControllerCallbackArgumentCaptor;


    SearchPresenterImpl presenter;
    private final static String SEARCH_TERM = "search term";


    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        presenter = new SearchPresenterImpl(mockView, mockSearchController, internetConnectionHelper);
        searchControllerCallbackArgumentCaptor = ArgumentCaptor.forClass(SearchControllerCallback.class);
    }

    @Test
    public void testInternetConnectionChecking() {
        when(internetConnectionHelper.isInternetAvailable()).thenReturn(false);

        presenter.onSearchRequested(SEARCH_TERM);


        verify(internetConnectionHelper).isInternetAvailable();
        verify(mockView, never()).showWaitingView();
        verify(mockSearchController, never()).search(anyString(), anyInt(), searchControllerCallbackArgumentCaptor.capture());


        assertEquals(0, presenter.getPageNumber());
    }

    @Test
    public void testOnSearchRequested() {
        when(internetConnectionHelper.isInternetAvailable()).thenReturn(true);

        presenter.onSearchRequested(SEARCH_TERM);

        verify(internetConnectionHelper).isInternetAvailable();
        verify(mockView).showWaitingView();
        verify(mockSearchController).search(anyString(), anyInt(), searchControllerCallbackArgumentCaptor.capture());

        assertEquals(1, presenter.getPageNumber());

    }

    @Test
    public void testOnLoadData() {
        when(internetConnectionHelper.isInternetAvailable()).thenReturn(true);

        presenter.onSearchRequested(SEARCH_TERM);
        presenter.onLoadData();

        verify(internetConnectionHelper, times(2)).isInternetAvailable();
        verify(mockSearchController, times(2)).search(anyString(), anyInt(), searchControllerCallbackArgumentCaptor.capture());

        ArrayList<SearchResultItem> searchResultItems = new ArrayList<>();
        searchResultItems.add(new SearchResultItem(1, "name", new Owner("url"), "des", 1, "url", "url"));
        SearchResult result = new SearchResult(1, searchResultItems);

        //searchControllerCallbackArgumentCaptor.getValue().onSearchResultReceived(result);
        searchControllerCallbackArgumentCaptor.getValue().onError("Error");
        verify(mockView).showErrorMessage("Error");

        assertEquals(2, presenter.getPageNumber());
    }

    @Test
    public void testSearchResultMapping() {
        Owner owner = new Owner("url");
        SearchResultItem searchResultItem = new SearchResultItem(1, "name", owner, "desc", 1, "sub_url", "url");
        ArrayList<SearchResultItem> searchResultItemList = new ArrayList<>();
        searchResultItemList.add(searchResultItem);
        SearchResult searchResult = new SearchResult(1, searchResultItemList);


        ArrayList<Repo> repoList = presenter.mapSearchResult(searchResult);
        Repo repo = repoList.get(0);

        assertEquals(1, repoList.size());
        assertEquals(searchResultItem.getId(), repo.getId());
        assertEquals(searchResultItem.getOwner().getAvatarUrl(), repo.getAvatarUrl());
        assertEquals(searchResultItem.getName(), repo.getRepoName());
        assertEquals(searchResultItem.getDescription(), repo.getDescription());
        assertEquals(searchResultItem.getForksCount(), repo.getForkCount());
        assertEquals(searchResultItem.getSubscribersUrl(), repo.getSubscribersUrl());
        assertEquals(searchResultItem.getUrl(), repo.getUrl());
    }
}