package com.trainingjourney.leo.asanarebelcodechallenge.ui.repodetails.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.trainingjourney.leo.asanarebelcodechallenge.R;
import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.entity.Subscriber;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SubscriberAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<Subscriber> dataSet;
    private SubscriberAdapterCallback searchAdapterCallback;
    private Context context;

    private boolean isLoaderShowing;

    private static final int VIEW_TYPE_ITEM = 1;
    private static final int VIEW_TYPE_LOADER = 2;

    public class LoaderViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.progressbar)
        ProgressBar progressBar;

        public LoaderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    public class SubscriberViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_view_subscriber_name)
        TextView textViewSubscriberName;

        @BindView(R.id.image_view_avatar)
        ImageView imageViewAvatar;

        View view;

        public SubscriberViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);
            this.view = view;
        }

        private void bind(final int itemIndex, final SubscriberAdapterCallback listener) {
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(dataSet.get(itemIndex));
                }
            });
        }
    }

    public SubscriberAdapter(Context context, ArrayList<Subscriber> dataSet, SubscriberAdapterCallback searchAdapterCallback) {
        this.dataSet = dataSet;
        this.context = context;
        this.searchAdapterCallback = searchAdapterCallback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {

        if (viewType == VIEW_TYPE_LOADER) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_loader_layout, parent, false);

            return new LoaderViewHolder(view);

        } else if (viewType == VIEW_TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_subscriber_layout, parent, false);

            return new SubscriberViewHolder(v);
        }

        throw new IllegalArgumentException("Invalid ViewType: " + viewType);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof LoaderViewHolder)
            bindLoaderViewHolder(viewHolder);

        else if (viewHolder instanceof SubscriberViewHolder)
            bindSubscriberViewHolder(viewHolder,position);

    }

    private void bindLoaderViewHolder(RecyclerView.ViewHolder viewHolder) {
        LoaderViewHolder loaderViewHolder = (LoaderViewHolder) viewHolder;
        if (isLoaderShowing)
            loaderViewHolder.progressBar.setVisibility(View.VISIBLE);
        else
            loaderViewHolder.progressBar.setVisibility(View.GONE);
    }

    private void bindSubscriberViewHolder(RecyclerView.ViewHolder viewHolder,int position){
        SubscriberViewHolder holder = (SubscriberViewHolder) viewHolder;
        holder.textViewSubscriberName.setText(dataSet.get(position).getLogin());
        Glide.with(context)
                .load(dataSet.get(position).getAvatarUrl())
                .apply(RequestOptions.circleCropTransform())
                .into(holder.imageViewAvatar);

        holder.bind(position, searchAdapterCallback);
    }
    @Override
    public int getItemCount() {
        if (dataSet == null || dataSet.size() == 0) {
            return 0;
        }
        return dataSet.size() + 1;
    }

    public int getItemViewType(int position) {
        if (position != 0 && position == getItemCount() - 1) {
            return VIEW_TYPE_LOADER;
        }

        return VIEW_TYPE_ITEM;
    }

    public void showLoading(boolean status) {
        isLoaderShowing = status;
    }

    public void append(ArrayList<Subscriber> newData) {
        dataSet.addAll(newData);
        notifyDataSetChanged();
    }


}
