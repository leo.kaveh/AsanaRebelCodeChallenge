package com.trainingjourney.leo.asanarebelcodechallenge.ui.search;


import com.trainingjourney.leo.asanarebelcodechallenge.ui.search.adapter.Repo;

import java.util.ArrayList;

public interface SearchContract {
    interface View{

        void showSearchResultCount(int resultCount);

        void showInternetNotAvailableError();

        void showWaitingView();

        void hideWaitingView();

        void showSearchResult(ArrayList<Repo> repoList);

        void showErrorMessage(String message);

        void showMoreSearchResult(ArrayList<Repo> repoList);

        void clearOldResults();

    }

    interface Presenter{
        void onSearchRequested(String searchText);

        void onLoadData();
    }
}
