package com.trainingjourney.leo.asanarebelcodechallenge.data.remote.controller.subscriber;


public interface SubscriberController {
    void getRepoSubscribers(String subscribersUrl,int pageNumber,SubscriberControllerCallback subscriberControllerCallback);
}
