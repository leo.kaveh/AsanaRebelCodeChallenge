package com.trainingjourney.leo.asanarebelcodechallenge.data.remote.entity;


public class RepoInfo extends SearchResultItem {
    private int subscribers_count;


    public RepoInfo(int id, String name, Owner owner, String description, int forks_count, String subscribers_url, String url,int subscribers_count) {
        super(id, name, owner, description, forks_count, subscribers_url, url);
        this.subscribers_count= subscribers_count;
    }

    public int getSubscribersCount() {
        return subscribers_count;
    }

}
