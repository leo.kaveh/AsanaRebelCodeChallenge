package com.trainingjourney.leo.asanarebelcodechallenge.data.remote.controller.subscriber;

import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.api.ApiClient;
import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.entity.Subscriber;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubscriberControllerImpl implements SubscriberController {

    private ApiClient apiClient;

    public SubscriberControllerImpl(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    @Override
    public void getRepoSubscribers(String subscribersUrl,int pageNumber, final SubscriberControllerCallback subscriberControllerCallback) {
        apiClient.getApiService().getRepoSubscriber(subscribersUrl,pageNumber).enqueue(new Callback<ArrayList<Subscriber>>() {
            @Override
            public void onResponse(Call<ArrayList<Subscriber>> call, Response<ArrayList<Subscriber>> response) {
                if (response.isSuccessful())
                    subscriberControllerCallback.onSubscribersListReceived(response.body());
                else
                    subscriberControllerCallback.onError("Something is wrong. Error Code:" + response.code());
            }

            @Override
            public void onFailure(Call<ArrayList<Subscriber>> call, Throwable t) {
                subscriberControllerCallback.onError(t.getMessage());
            }
        });
    }
}
