package com.trainingjourney.leo.asanarebelcodechallenge.data.remote.controller.search;

public interface SearchController {
    void search(String searchText,int pageNumber, SearchControllerCallback searchControllerCallback);
}
