package com.trainingjourney.leo.asanarebelcodechallenge.data.remote.entity;


import java.util.ArrayList;

public class SearchResult {
    private int total_count;
    private ArrayList<SearchResultItem> items;

    public SearchResult(int total_count, ArrayList<SearchResultItem> searchResultItems) {
        this.total_count = total_count;
        this.items = searchResultItems;
    }

    public int getTotalCount() {
        return total_count;
    }

    public ArrayList<SearchResultItem> getSearchResultItems() {
        return items;
    }

}
