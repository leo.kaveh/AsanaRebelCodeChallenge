package com.trainingjourney.leo.asanarebelcodechallenge.ui.repodetails.adapter;


import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.entity.Subscriber;

public interface SubscriberAdapterCallback {
    void onItemClick(Subscriber subscriber);
}
