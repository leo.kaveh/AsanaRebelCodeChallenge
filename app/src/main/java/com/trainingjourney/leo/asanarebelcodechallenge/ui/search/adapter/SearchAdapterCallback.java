package com.trainingjourney.leo.asanarebelcodechallenge.ui.search.adapter;


public interface SearchAdapterCallback {
    void onItemClick(Repo repo);
}
