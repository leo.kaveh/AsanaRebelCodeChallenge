package com.trainingjourney.leo.asanarebelcodechallenge.data.remote.api;


import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.entity.RepoInfo;
import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.entity.SearchResult;
import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.entity.Subscriber;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface ApiService {

    @GET("/search/repositories")
    Call<SearchResult> search(@Query("q") String searchText,
                              @Query("sort") String sort,
                              @Query("order") String order,
                              @Query("page") int pageNumber);

    @GET
    Call<ArrayList<Subscriber>> getRepoSubscriber(@Url String url,
                                                  @Query("page") int pageNumber);

    @GET
    Call<RepoInfo> getRepoInfo(@Url String url);
}
