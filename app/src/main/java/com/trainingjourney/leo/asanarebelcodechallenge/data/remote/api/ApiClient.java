package com.trainingjourney.leo.asanarebelcodechallenge.data.remote.api;



public interface ApiClient {
    ApiService getApiService();
}