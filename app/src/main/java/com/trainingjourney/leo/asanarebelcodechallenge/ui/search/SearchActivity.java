package com.trainingjourney.leo.asanarebelcodechallenge.ui.search;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.trainingjourney.leo.asanarebelcodechallenge.R;
import com.trainingjourney.leo.asanarebelcodechallenge.ui.repodetails.RepoDetailsActivity;
import com.trainingjourney.leo.asanarebelcodechallenge.ui.search.adapter.Repo;
import com.trainingjourney.leo.asanarebelcodechallenge.ui.search.adapter.SearchAdapter;
import com.trainingjourney.leo.asanarebelcodechallenge.ui.search.adapter.SearchAdapterCallback;
import com.trainingjourney.leo.asanarebelcodechallenge.util.DependencyContainer;
import com.trainingjourney.leo.asanarebelcodechallenge.util.EndlessRecyclerViewScrollListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchActivity extends AppCompatActivity implements SearchContract.View{

    @BindView(R.id.progressbar)
    public ProgressBar progressbar;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.edit_text_search)
    EditText editTextSearch;

    @BindView(R.id.text_view_result_count)
    TextView textViewResultCount;

    SearchContract.Presenter presenter;
    SearchAdapter searchAdapter;


    public interface ProgressListener {
        void onProgressShown();

        void onProgressDismissed();
    }

    private ProgressListener testProgressListener;

    public void setProgressListener(ProgressListener progressListener) {
        testProgressListener = progressListener;
    }

    private void notifyListener(ProgressListener listener) {
        if (listener == null) {
            return;
        }
        if (isInProgress()) {
            listener.onProgressShown();
        } else {
            listener.onProgressDismissed();
        }
    }

    public boolean isInProgress() {
        return progressbar.getVisibility() == View.VISIBLE;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);

        initViews();

        DependencyContainer dependencyContainer = DependencyContainer.getInstance();
        presenter = new SearchPresenterImpl(this,
                dependencyContainer.getSearchApi(),
                dependencyContainer.getInternetConnectionHelper(this));
    }

    private void initViews() {
        initRecyclerView();
        initSearchEditText();

        progressbar.setVisibility(View.INVISIBLE);
        notifyListener(testProgressListener);
    }

    private void initRecyclerView(){
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                presenter.onLoadData();
                searchAdapter.showLoading(true);
                searchAdapter.notifyDataSetChanged();
            }
        });
    }

    private void initSearchEditText(){
        editTextSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    presenter.onSearchRequested(editTextSearch.getText().toString());
                    hideKeyboard();
                    return true;
                }
                return false;
            }

        });

    }

    @Override
    public void showSearchResultCount(int resultCount) {
        textViewResultCount.setVisibility(View.VISIBLE);
        textViewResultCount.setText("Result Found: "+resultCount);
    }

    @Override
    public void showInternetNotAvailableError() {
        Toast.makeText(this, "Internet Connection is NOT available", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showWaitingView() {
        progressbar.setVisibility(View.VISIBLE);
        notifyListener(testProgressListener);
    }

    @Override
    public void hideWaitingView() {
        progressbar.setVisibility(View.INVISIBLE);
        notifyListener(testProgressListener);
    }

    @Override
    public void showSearchResult(ArrayList<Repo> repoList) {
        searchAdapter = new SearchAdapter(this, repoList, new SearchAdapterCallback() {
            @Override
            public void onItemClick(Repo repo) {
                Intent intent = new Intent(SearchActivity.this, RepoDetailsActivity.class);
                intent.putExtra(RepoDetailsActivity.REPO_KEY, repo);
                startActivity(intent);
            }
        });
        recyclerView.setAdapter(searchAdapter);
    }

    @Override
    public void showErrorMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showMoreSearchResult(ArrayList<Repo> repoList) {
        searchAdapter.showLoading(false);
        searchAdapter.append(repoList);
    }

    @Override
    public void clearOldResults() {
        if (searchAdapter != null)
            searchAdapter.clear();
    }

    private void hideKeyboard(){
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
