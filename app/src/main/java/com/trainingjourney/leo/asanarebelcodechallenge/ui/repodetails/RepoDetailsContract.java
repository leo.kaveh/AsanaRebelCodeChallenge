package com.trainingjourney.leo.asanarebelcodechallenge.ui.repodetails;


import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.entity.Subscriber;
import com.trainingjourney.leo.asanarebelcodechallenge.ui.search.adapter.Repo;

import java.util.ArrayList;

public interface RepoDetailsContract {
    interface View {
        void showInternetNotAvailableError();

        void initViews(Repo repo);

        void showWaitingView();

        void hideWaitingView();

        void showSubscribers(ArrayList<Subscriber> subscribersList);

        void showMoreSubscribers(ArrayList<Subscriber> subscribersList);

        void showErrorMessage(String message);

        void showSubscribersCount(int subscribersCount);
    }

    interface Presenter {
        void onInit(Repo repo);

        void onLoadData();

        int getPageNumber();
    }
}
