package com.trainingjourney.leo.asanarebelcodechallenge.data.remote.controller.repoinfo;

public interface RepoInfoController {
    void getRepoInfo(String repoUrl, RepoInfoControllerCallback repoInfoControllerCallback);
}
