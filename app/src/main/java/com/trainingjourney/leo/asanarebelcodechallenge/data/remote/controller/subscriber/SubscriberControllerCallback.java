package com.trainingjourney.leo.asanarebelcodechallenge.data.remote.controller.subscriber;


import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.api.ApiCallback;
import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.entity.Subscriber;

import java.util.ArrayList;

public interface SubscriberControllerCallback extends ApiCallback {
    void onSubscribersListReceived(ArrayList<Subscriber> subscribersList);
}
