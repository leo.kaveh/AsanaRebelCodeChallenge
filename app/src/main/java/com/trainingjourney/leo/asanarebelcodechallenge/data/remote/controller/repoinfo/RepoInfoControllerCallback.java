package com.trainingjourney.leo.asanarebelcodechallenge.data.remote.controller.repoinfo;


import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.api.ApiCallback;
import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.entity.RepoInfo;

public interface RepoInfoControllerCallback extends ApiCallback {
    void onRepoInfoReceived(RepoInfo repoInfo);
}
