package com.trainingjourney.leo.asanarebelcodechallenge.data.remote.entity;



public class Subscriber {
    private String login,avatar_url,html_url;

    public Subscriber(String login, String avatar_url, String html_url) {
        this.login = login;
        this.avatar_url = avatar_url;
        this.html_url = html_url;
    }

    public String getLogin() {
        return login;
    }


    public String getAvatarUrl() {
        return avatar_url;
    }


    public String getHtmlUrl() {
        return html_url;
    }

}
