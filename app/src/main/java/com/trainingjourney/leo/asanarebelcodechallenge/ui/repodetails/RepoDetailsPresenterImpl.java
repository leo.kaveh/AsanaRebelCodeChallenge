package com.trainingjourney.leo.asanarebelcodechallenge.ui.repodetails;


import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.controller.repoinfo.RepoInfoController;
import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.controller.repoinfo.RepoInfoControllerCallback;
import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.controller.subscriber.SubscriberController;
import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.controller.subscriber.SubscriberControllerCallback;
import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.entity.RepoInfo;
import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.entity.Subscriber;
import com.trainingjourney.leo.asanarebelcodechallenge.ui.search.adapter.Repo;
import com.trainingjourney.leo.asanarebelcodechallenge.util.connectionhelper.InternetConnectionHelper;

import java.util.ArrayList;

public class RepoDetailsPresenterImpl implements RepoDetailsContract.Presenter {

    private RepoDetailsContract.View view;
    private RepoInfoController repoInfoController;
    private SubscriberController subscriberController;
    private InternetConnectionHelper internetConnectionHelper;

    private Repo repo;
    private int pageNumber;

    public RepoDetailsPresenterImpl(RepoDetailsContract.View view, RepoInfoController repoInfoController, SubscriberController subscriberController, InternetConnectionHelper internetConnectionHelper) {
        this.view = view;
        this.repoInfoController = repoInfoController;
        this.subscriberController = subscriberController;
        this.internetConnectionHelper = internetConnectionHelper;
    }

    @Override
    public void onInit(Repo repo) {
        this.repo = repo;
        view.initViews(repo);

        if(!internetConnectionHelper.isInternetAvailable()){
            view.showInternetNotAvailableError();
            return;
        }

        view.showWaitingView();

        getRepoInfo();
        getSubscribersList();
    }

    private void getRepoInfo() {
        repoInfoController.getRepoInfo(repo.getUrl(), new RepoInfoControllerCallback() {
            @Override
            public void onRepoInfoReceived(RepoInfo repoInfo) {
                view.showSubscribersCount(repoInfo.getSubscribersCount());
            }

            @Override
            public void onError(String errorMessage) {
                view.showErrorMessage(errorMessage);
            }
        });
    }

    private void getSubscribersList() {
        pageNumber = 1;
        subscriberController.getRepoSubscribers(repo.getSubscribersUrl(), pageNumber, new SubscriberControllerCallback() {
            @Override
            public void onSubscribersListReceived(ArrayList<Subscriber> subscribersList) {
                view.hideWaitingView();
                view.showSubscribers(subscribersList);
            }

            @Override
            public void onError(String errorMessage) {
                view.hideWaitingView();
                view.showErrorMessage(errorMessage);
            }
        });
    }


    @Override
    public void onLoadData() {
        if(!internetConnectionHelper.isInternetAvailable()){
            view.showInternetNotAvailableError();
            return;
        }

        pageNumber++;

        subscriberController.getRepoSubscribers(repo.getSubscribersUrl(),pageNumber, new SubscriberControllerCallback() {
            @Override
            public void onSubscribersListReceived(ArrayList<Subscriber> subscribersList) {
                view.showMoreSubscribers(subscribersList);
            }

            @Override
            public void onError(String errorMessage) {
                view.showErrorMessage(errorMessage);
            }
        });
    }

    @Override
    public int getPageNumber() {
        return pageNumber;
    }


}
