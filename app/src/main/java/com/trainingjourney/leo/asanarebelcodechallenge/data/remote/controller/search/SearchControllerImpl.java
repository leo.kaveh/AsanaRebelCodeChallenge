package com.trainingjourney.leo.asanarebelcodechallenge.data.remote.controller.search;


import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.api.ApiClient;
import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.entity.SearchResult;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchControllerImpl implements SearchController {
    private ApiClient apiClient;

    private static final String SORT = "stars";
    private static final String ORDER = "desc";

    public SearchControllerImpl(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    @Override
    public void search(String searchText, int pageNumber ,final SearchControllerCallback searchControllerCallback) {
        apiClient.getApiService().search(searchText, SORT, ORDER, pageNumber).enqueue(new Callback<SearchResult>() {
            @Override
            public void onResponse(Call<SearchResult> call, Response<SearchResult> response) {
                if (response.isSuccessful())
                    searchControllerCallback.onSearchResultReceived(response.body());
                else
                    searchControllerCallback.onError("Something is wrong. Error Code:" + response.code());
            }

            @Override
            public void onFailure(Call<SearchResult> call, Throwable t) {
                searchControllerCallback.onError(t.getMessage());
            }
        });
    }
}
