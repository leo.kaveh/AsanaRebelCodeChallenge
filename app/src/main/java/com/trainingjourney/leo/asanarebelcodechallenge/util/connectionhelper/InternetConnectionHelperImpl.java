package com.trainingjourney.leo.asanarebelcodechallenge.util.connectionhelper;

import android.content.Context;
import android.net.ConnectivityManager;

public class InternetConnectionHelperImpl implements InternetConnectionHelper {

    private Context context;

    public InternetConnectionHelperImpl(Context context) {
        this.context = context;
    }

    @Override
    public boolean isInternetAvailable() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }
}
