package com.trainingjourney.leo.asanarebelcodechallenge.util.connectionhelper;


public interface InternetConnectionHelper {
    boolean isInternetAvailable();
}
