package com.trainingjourney.leo.asanarebelcodechallenge.ui.repodetails;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.trainingjourney.leo.asanarebelcodechallenge.R;
import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.entity.Subscriber;
import com.trainingjourney.leo.asanarebelcodechallenge.ui.repodetails.adapter.SubscriberAdapter;
import com.trainingjourney.leo.asanarebelcodechallenge.ui.repodetails.adapter.SubscriberAdapterCallback;
import com.trainingjourney.leo.asanarebelcodechallenge.ui.search.adapter.Repo;
import com.trainingjourney.leo.asanarebelcodechallenge.util.DependencyContainer;
import com.trainingjourney.leo.asanarebelcodechallenge.util.EndlessRecyclerViewScrollListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RepoDetailsActivity extends AppCompatActivity implements RepoDetailsContract.View {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.progressbar)
    ProgressBar progressBar;

    @BindView(R.id.image_view_avatar)
    ImageView imageViewAvatar;

    @BindView(R.id.text_view_repo_name)
    TextView textViewRepoName;

    @BindView(R.id.text_view_description)
    TextView textViewDescription;

    @BindView(R.id.text_view_subscribers_count)
    TextView textViewSubscribersCount;

    @BindView(R.id.text_view_subscribers)
    TextView textViewSubscribers;

    public static final String REPO_KEY = "repoKey";
    private RepoDetailsContract.Presenter presenter;
    private SubscriberAdapter subscriberAdapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repo_details);
        ButterKnife.bind(this);

        Bundle data = getIntent().getExtras();
        Repo repo = data.getParcelable(REPO_KEY);

        DependencyContainer dependencyContainer = DependencyContainer.getInstance();
        presenter = new RepoDetailsPresenterImpl(this,
                dependencyContainer.getRepoInfoController(),
                dependencyContainer.getSubscriberController(),
                dependencyContainer.getInternetConnectionHelper(this));

        presenter.onInit(repo);
    }

    @Override
    public void initViews(Repo repo) {
        Glide.with(this)
                .load(repo.getAvatarUrl())
                .apply(RequestOptions.circleCropTransform())
                .into(imageViewAvatar);

        textViewRepoName.setText(repo.getRepoName());
        textViewDescription.setText(repo.getDescription());

        initRecyclerView();
    }

    private void initRecyclerView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                presenter.onLoadData();
                subscriberAdapter.showLoading(true);
                subscriberAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void showWaitingView() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideWaitingView() {
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showSubscribers(ArrayList<Subscriber> subscribersList) {
        subscriberAdapter = new SubscriberAdapter(this, subscribersList, new SubscriberAdapterCallback() {
            @Override
            public void onItemClick(Subscriber subscriber) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(subscriber.getHtmlUrl()));
                startActivity(browserIntent);
            }
        });
        recyclerView.setAdapter(subscriberAdapter);
    }

    @Override
    public void showMoreSubscribers(ArrayList<Subscriber> subscribersList) {
        subscriberAdapter.showLoading(false);
        subscriberAdapter.append(subscribersList);
    }

    @Override
    public void showErrorMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showSubscribersCount(int subscribersCount) {
        textViewSubscribers.setVisibility(View.VISIBLE);
        textViewSubscribersCount.setVisibility(View.VISIBLE);
        textViewSubscribersCount.setText(String.valueOf(subscribersCount));
    }

    @Override
    public void showInternetNotAvailableError() {
        Toast.makeText(this, "Internet Connection is NOT available", Toast.LENGTH_SHORT).show();
    }
}
