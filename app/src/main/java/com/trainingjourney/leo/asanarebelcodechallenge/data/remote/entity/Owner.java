package com.trainingjourney.leo.asanarebelcodechallenge.data.remote.entity;


public class Owner {
    private String avatar_url;

    public Owner(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    public String getAvatarUrl() {
        return avatar_url;
    }
}
