package com.trainingjourney.leo.asanarebelcodechallenge.data.remote.entity;


public class SearchResultItem {
    private int id;
    private String name;
    private Owner owner;
    private String description;
    private int forks_count;
    private String subscribers_url;
    private String url;

    public SearchResultItem(int id, String name, Owner owner, String description, int forks_count, String subscribers_url,String url) {
        this.id = id;
        this.name = name;
        this.owner = owner;
        this.description = description;
        this.forks_count = forks_count;
        this.subscribers_url = subscribers_url;
        this.url = url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Owner getOwner() {
        return owner;
    }

    public String getDescription() {
        return description;
    }

    public int getForksCount() {
        return forks_count;
    }

    public String getSubscribersUrl() {
        return subscribers_url;
    }

    public String getUrl() {
        return url;
    }

}
