package com.trainingjourney.leo.asanarebelcodechallenge.data.remote.api;


import com.google.gson.Gson;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClientImpl implements ApiClient {


    private final static String BASE_URL = "https://api.github.com";

    @Override

    public ApiService getApiService() {
        return getRetrofit().create(ApiService.class);
    }

    private Retrofit getRetrofit() {
        return (new Retrofit.Builder())
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .build();
    }
}
