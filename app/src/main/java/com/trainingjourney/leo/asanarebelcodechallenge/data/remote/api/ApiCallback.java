package com.trainingjourney.leo.asanarebelcodechallenge.data.remote.api;


public interface ApiCallback {
    void onError(String errorMessage);

}
