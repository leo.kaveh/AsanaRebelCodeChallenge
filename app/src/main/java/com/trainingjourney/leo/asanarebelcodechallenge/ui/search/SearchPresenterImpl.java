package com.trainingjourney.leo.asanarebelcodechallenge.ui.search;


import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.controller.search.SearchController;
import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.controller.search.SearchControllerCallback;
import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.entity.SearchResult;
import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.entity.SearchResultItem;
import com.trainingjourney.leo.asanarebelcodechallenge.ui.search.adapter.Repo;
import com.trainingjourney.leo.asanarebelcodechallenge.util.connectionhelper.InternetConnectionHelper;

import java.util.ArrayList;

public class SearchPresenterImpl implements SearchContract.Presenter {

    private SearchContract.View view;
    private SearchController searchController;
    private InternetConnectionHelper internetConnectionHelper;

    private String searchText;
    private int pageNumber;

    public SearchPresenterImpl(SearchContract.View view, SearchController searchController, InternetConnectionHelper internetConnectionHelper) {
        this.view = view;
        this.searchController = searchController;
        this.internetConnectionHelper = internetConnectionHelper;
    }

    @Override
    public void onSearchRequested(String searchText) {
        if(!internetConnectionHelper.isInternetAvailable()){
            view.showInternetNotAvailableError();
            return;
        }

        view.clearOldResults();
        view.showWaitingView();

        pageNumber = 1;
        this.searchText = searchText;

        getSearchResult();
    }

    private void getSearchResult(){
        searchController.search(searchText, pageNumber, new SearchControllerCallback() {
            @Override
            public void onSearchResultReceived(SearchResult result) {
                view.hideWaitingView();
                ArrayList<Repo> repoList = mapSearchResult(result);
                view.showSearchResultCount(result.getTotalCount());
                view.showSearchResult(repoList);
            }

            @Override
            public void onError(String errorMessage) {
                view.hideWaitingView();
                view.showErrorMessage(errorMessage);
            }
        });
    }

    @Override
    public void onLoadData() {
        if(!internetConnectionHelper.isInternetAvailable()){
            view.showInternetNotAvailableError();
            return;
        }

        getMoreSearchResult();
    }

    private void getMoreSearchResult(){
        pageNumber++;

        searchController.search(searchText, pageNumber, new SearchControllerCallback() {
            @Override
            public void onSearchResultReceived(SearchResult result) {
                ArrayList<Repo> repoList = mapSearchResult(result);
                view.showMoreSearchResult(repoList);
            }

                @Override
            public void onError(String errorMessage) {
                view.showErrorMessage(errorMessage);
            }
        });
    }
    public int getPageNumber() {
        return pageNumber;
    }

    public ArrayList<Repo> mapSearchResult(SearchResult searchResult) {
        ArrayList<Repo> repoListItemsList = new ArrayList<>();

        for (SearchResultItem item : searchResult.getSearchResultItems()) {
            Repo repo = new Repo(item.getId(),
                    item.getOwner().getAvatarUrl(),
                    item.getName(),
                    item.getDescription(),
                    item.getForksCount(),
                    item.getSubscribersUrl(),
                    item.getUrl());


            repoListItemsList.add(repo);
        }

        return repoListItemsList;
    }
}
