package com.trainingjourney.leo.asanarebelcodechallenge.ui.search.adapter;


import android.os.Parcel;
import android.os.Parcelable;

public class Repo implements Parcelable {
    private String avatarUrl, repoName, description, subscribersUrl,url;
    private int forkCount,id;

    public Repo(int id, String avatarUrl, String repoName, String description, int forkCount, String subscribersUrl,String url) {
        this.id = id;
        this.avatarUrl = avatarUrl;
        this.repoName = repoName;
        this.description = description;
        this.forkCount = forkCount;
        this.subscribersUrl = subscribersUrl;
        this.url = url;
    }

    protected Repo(Parcel in) {
        avatarUrl = in.readString();
        repoName = in.readString();
        description = in.readString();
        forkCount = in.readInt();
        id = in.readInt();
        subscribersUrl = in.readString();
        url = in.readString();
    }

    public static final Creator<Repo> CREATOR = new Creator<Repo>() {
        @Override
        public Repo createFromParcel(Parcel in) {
            return new Repo(in);
        }

        @Override
        public Repo[] newArray(int size) {
            return new Repo[size];
        }
    };

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public String getRepoName() {
        return repoName;
    }

    public String getDescription() {
        return description;
    }

    public int getForkCount() {
        return forkCount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSubscribersUrl() {
        return subscribersUrl;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(avatarUrl);
        parcel.writeString(repoName);
        parcel.writeString(description);
        parcel.writeInt(forkCount);
        parcel.writeInt(id);
        parcel.writeString(subscribersUrl);
        parcel.writeString(url);
    }
}
