package com.trainingjourney.leo.asanarebelcodechallenge.util;


import android.content.Context;

import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.api.ApiClient;
import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.api.ApiClientImpl;
import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.controller.repoinfo.RepoInfoController;
import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.controller.repoinfo.RepoInfoControllerImpl;
import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.controller.search.SearchController;
import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.controller.search.SearchControllerImpl;
import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.controller.subscriber.SubscriberController;
import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.controller.subscriber.SubscriberControllerImpl;
import com.trainingjourney.leo.asanarebelcodechallenge.util.connectionhelper.InternetConnectionHelper;
import com.trainingjourney.leo.asanarebelcodechallenge.util.connectionhelper.InternetConnectionHelperImpl;

public class DependencyContainer {
    private static DependencyContainer instance;

    private DependencyContainer() {

    }

    public static DependencyContainer getInstance() {
        if (instance == null)
            instance = new DependencyContainer();

        return instance;
    }

    private ApiClient getApiClient() {
        return new ApiClientImpl();
    }

    public SearchController getSearchApi() {
        return new SearchControllerImpl(getApiClient());
    }

    public SubscriberController getSubscriberController() {
        return new SubscriberControllerImpl(getApiClient());
    }

    public RepoInfoController getRepoInfoController() {
        return new RepoInfoControllerImpl(getApiClient());
    }

    public InternetConnectionHelper getInternetConnectionHelper(Context context) {
        return new InternetConnectionHelperImpl(context);
    }
}
