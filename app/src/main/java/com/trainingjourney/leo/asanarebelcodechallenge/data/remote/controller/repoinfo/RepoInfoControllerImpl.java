package com.trainingjourney.leo.asanarebelcodechallenge.data.remote.controller.repoinfo;


import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.api.ApiClient;
import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.entity.RepoInfo;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RepoInfoControllerImpl implements RepoInfoController {
    private ApiClient apiClient;

    public RepoInfoControllerImpl(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    @Override
    public void getRepoInfo(String repoUrl, final RepoInfoControllerCallback repoInfoControllerCallback) {
        apiClient.getApiService().getRepoInfo(repoUrl).enqueue(new Callback<RepoInfo>() {
            @Override
            public void onResponse(Call<RepoInfo> call, Response<RepoInfo> response) {
                if (response.isSuccessful())
                    repoInfoControllerCallback.onRepoInfoReceived(response.body());
                else
                    repoInfoControllerCallback.onError("Something is wrong. Error Code:" + response.code());
            }

            @Override
            public void onFailure(Call<RepoInfo> call, Throwable t) {
                repoInfoControllerCallback.onError(t.getMessage());
            }
        });
    }
}
