package com.trainingjourney.leo.asanarebelcodechallenge.data.remote.controller.search;


import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.api.ApiCallback;
import com.trainingjourney.leo.asanarebelcodechallenge.data.remote.entity.SearchResult;

public interface SearchControllerCallback extends ApiCallback{

    void onSearchResultReceived(SearchResult result);

}
