package com.trainingjourney.leo.asanarebelcodechallenge;


import android.support.test.espresso.Espresso;
import android.support.test.espresso.IdlingResource;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.trainingjourney.leo.asanarebelcodechallenge.ui.search.SearchActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.pressImeActionButton;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.trainingjourney.leo.asanarebelcodechallenge.TestUtils.withRecyclerView;

@RunWith(AndroidJUnit4.class)
public class SearchActivityTest {

    private IdlingResource mIdlingResource;

    @Rule
    public ActivityTestRule<SearchActivity> activityRule = new ActivityTestRule(SearchActivity.class);

    @Before
    public void setup() {
        mIdlingResource = new ProgressIdlingResource(activityRule.getActivity());
        Espresso.registerIdlingResources(mIdlingResource);
    }
    @Test
    public void listGoesOverTheFold() {
        onView(withId(R.id.edit_text_search)).check(matches(isDisplayed()));
        onView(withId(R.id.progressbar)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.INVISIBLE)));

        onView(withId(R.id.edit_text_search)).perform(click()).perform(typeText("android"), closeSoftKeyboard()).perform(pressImeActionButton());
        onView(withId(R.id.recycler_view)).check(matches(isDisplayed()));
        onView(withId(R.id.recycler_view)).check(matches(withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)));

        onView(withRecyclerView(R.id.recycler_view)
                .atPositionOnView(0, R.id.text_view_repo_name))
                .check(matches(withText("material-design-icons")));
    }



}


