package com.trainingjourney.leo.asanarebelcodechallenge;


import android.support.test.espresso.IdlingResource;

import com.trainingjourney.leo.asanarebelcodechallenge.ui.search.SearchActivity;

public class ProgressIdlingResource implements IdlingResource {

    private ResourceCallback resourceCallback;
    private SearchActivity searchActivity;
    private SearchActivity.ProgressListener progressListener;

    public ProgressIdlingResource(SearchActivity activity){
        searchActivity = activity;

        progressListener = new SearchActivity.ProgressListener() {
            @Override
            public void onProgressShown() {
            }
            @Override
            public void onProgressDismissed() {
                if (resourceCallback == null){
                    return ;
                }
                //Called when the resource goes from busy to idle.
                resourceCallback.onTransitionToIdle();
            }
        };

        searchActivity.setProgressListener (progressListener);
    }
    @Override
    public String getName() {
        return "My idling resource";
    }

    @Override
    public boolean isIdleNow() {
        // the resource becomes idle when the progress has been dismissed
        return !searchActivity.isInProgress();
    }

    @Override
    public void registerIdleTransitionCallback(ResourceCallback resourceCallback) {
        this.resourceCallback = resourceCallback;
    }
}